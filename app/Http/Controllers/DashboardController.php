<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\SecEmployee;
use App\Models\SecUsers;
use App\Models\SysPengunjung;

class DashboardController extends Controller
{

    
    public function __construct()
    {
       
    }

    public function index() {
        $count_employee     = SecEmployee::join('sec_users','sec_users.id','sec_employee.user_id')->whereNull('deleted_at')->count();
        $count_pengunjung   = SysPengunjung::count();

        return view('dashboard')->with([
            'count_employee'    => $count_employee,
            'count_pengunjung'  => $count_pengunjung
        ]);
    }
}
