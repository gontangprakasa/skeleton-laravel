<?php
	$currentAction = Route::currentRouteAction();
	list($controller, $method) = explode('@', $currentAction);
	$controller = str_replace("App\\Http\\Controllers\\", "", $controller);
?>
<section class="sidebar">
	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<li class="{{ ($controller=='DashboardController'?'active':'') }}">
			<a href="{{ url('dashboard') }}">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
			</a>
		</li>

		@if(in_array(Auth::User()->role_id, [1]))
            <li class="treeview {{ (Request::segment(2)=='halaman'?'active':'') }}">
                <a href="#">
                    <i class="fa fa-archive"></i> <span>Halaman Sistem</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>

                <ul class="treeview-menu">
                    <li class="{{ (Request::segment(3)=='informasi'?'active':'') }}">
                        <a href="{{ url('admin/halaman/informasi') }}">
                            <i class="fa fa-circle-o"></i> <span>Informasi</span>
                        </a>
                    </li>
                </ul>
            </li>
        @endif
        @if(in_array(Auth::User()->role_id, [1]))
            <li class="treeview {{ (Request::segment(2)=='master'?'active':'') }}">
                <a href="#">
                    <i class="fa fa-archive"></i> <span>Master</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>

                <ul class="treeview-menu">
                    <li class="{{ (Request::segment(3)=='jenis-pekerjaan'?'active':'') }}">
                        <a href="{{ url('admin/master/jenis-pekerjaan') }}">
                            <i class="fa fa-circle-o"></i> <span>Jenis Pekerjaan</span>
                        </a>
                    </li>

                    <li class="{{ (Request::segment(3)=='jenis-project'?'active':'') }}">
                        <a href="{{ url('admin/master/jenis-project') }}">
                            <i class="fa fa-circle-o"></i> <span>Jenis Project</span>
                        </a>
                    </li>

                    <li class="{{ (Request::segment(3)=='keahlian'?'active':'') }}">
                        <a href="{{ url('admin/master/keahlian') }}">
                            <i class="fa fa-circle-o"></i> <span>Keahlian</span>
                        </a>
                    </li>

                    <li class="{{ (Request::segment(3)=='negara'?'active':'') }}">
                        <a href="{{ url('admin/master/negara') }}">
                            <i class="fa fa-circle-o"></i> <span>Negara</span>
                        </a>
                    </li>

                    <li class="{{ (Request::segment(3)=='pendidikan'?'active':'') }}">
                        <a href="{{ url('admin/master/pendidikan') }}">
                            <i class="fa fa-circle-o"></i> <span>Pendidikan</span>
                        </a>
                    </li>

                    @if(in_array(Auth::User()->role_id, [1]))
                        <li class="{{ (Request::segment(3)=='role'?'active':'') }}">
                            <a href="{{ url('admin/master/role') }}">
                                <i class="fa fa-circle-o"></i> <span>Role</span>
                            </a>
                        </li>
                    @endif

                    <li class="{{ (Request::segment(3)=='social-media'?'active':'') }}">
                        <a href="{{ url('admin/master/social-media') }}">
                            <i class="fa fa-circle-o"></i> <span>Social Media</span>
                        </a>
                    </li>
                </ul>
            </li>
        @endif

        <hr>

        @if(in_array(Auth::User()->role_id, [1]))
            <li class="treeview {{ (Request::segment(2)=='employee'?'active':'') }}">
                <a href="#">
                    <i class="fa fa-archive"></i> <span>Employee</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>

                <ul class="treeview-menu">
                    <li class="{{ (Request::segment(3)=='active'?'active':'') }}">
                        <a href="{{ url('admin/employee/active') }}">
                            <i class="fa fa-circle-o"></i> <span>Employee Active</span>
                        </a>
                    </li>

                    <li class="{{ (Request::segment(3)=='non-active'?'active':'') }}">
                        <a href="{{ url('admin/employee/non-active') }}">
                            <i class="fa fa-circle-o"></i> <span>Employee Non-Active</span>
                        </a>
                    </li>
                </ul>
            </li>
        @endif
        <hr>
		
	</ul>
</section>