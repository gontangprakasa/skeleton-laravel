<!DOCTYPE html>
<html lang="en">

<head>
  	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ env("APP_NAME") }} | @yield('title')</title>

	<!-- Favicons -->
	<link rel="stylesheet" href="{{ asset('web/img/favicon.png') }}" rel="icon">
	<link rel="stylesheet" href="{{ asset('web/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link rel="stylesheet" href="{{ asset('web/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('web/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('web/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('web/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('web/vendor/venobox/venobox.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('web/vendor/aos/aos.css') }}" rel="stylesheet">

	<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2-bootstrap.min.css') }}">
	<!-- Template Main CSS File -->
	<link rel="stylesheet" href="{{ asset('web/css/style.css') }}" rel="stylesheet">

	@yield('contentCss')
</head>

<body>
	<header>
		<div class="sidebar-collapse">
			<nav class="navbar navbar-expand-lg fixed-top navbar-transparent" color-on-scroll="400">
			<div class="container">
				<div class="navbar-translate"><a class="navbar-brand" href="#" rel="tooltip">{{ env("APP_NAME") }}</a>
				<button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-bar bar1"></span><span class="navbar-toggler-bar bar2"></span><span class="navbar-toggler-bar bar3"></span></button>
				</div>
				<div class="collapse navbar-collapse justify-content-end" id="navigation">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link smooth-scroll" href="#about">About</a></li>
					<li class="nav-item"><a class="nav-link smooth-scroll" href="#skill">Skills</a></li>
					
					<li class="nav-item"><a class="nav-link smooth-scroll" href="#experience">Project</a></li>
					<li class="nav-item"><a class="nav-link smooth-scroll" href="#portfolio">Work Experience</a></li>
					<li class="nav-item"><a class="nav-link smooth-scroll" href="#contact">Contact</a></li>
					<li class="nav-item"><a class="nav-link smooth-scroll" href="https://gontangprakasa02.blogspot.com" target="_blank">Blog</a></li>
				</ul>
				</div>
			</div>
			</nav>
		</div>
    </header>

	<!-- ======= Hero Section ======= -->
	<section class="align-items-center"></section>
  	<!-- End Hero -->

  	@yield('content')

	<footer id="footer">
		<div class="footer-top">
			<div class="container">
				<div class="row">

					<div class="col-lg-4 col-md-6 footer-contact">
						<h3>Data Pribadi</h3>
						<p>
						{{ @$data->alamat_cipta }}<br><br>
						{{ @$data->negara_cipta }} <br><br>
						<strong>Phone :</strong> {{ @$data->telp_cipta }}<br>
						<strong>Email :</strong> {{ @$data->email_cipta }}<br>
						</p>
					</div>

					<div class="col-lg-4 col-md-6 footer-links">
						
					</div>

					<div class="col-lg-4 col-md-6 footer-links">
						<h4>Sosial Media</h4>
						<div class="social-links mt-3">
							@if (@$data->twitter_cipta)
								<a href="{{ @$data->twitter_cipta }}" class="twitter" target="_blank"><i class="bx bxl-twitter"></i></a>
							@endif
							@if (@$data->facebook_cipta)
								<a href="{{ @$data->facebook_cipta }}" class="facebook"><i class="bx bxl-facebook"></i></a>
							@endif
							@if (@$data->instagram_cipta)
								<a href="{{ @$data->instagram_cipta }}" class="instagram"><i class="bx bxl-instagram"></i></a>
							@endif
							@if (@$data->linkedin_cipta)
								<a href="{{ @$data->linkedin_cipta }}" class="linkedin"><i class="bx bxl-linkedin"></i></a>
							@endif
							@if (@$data->whatsapp_cipta)
								<a href="{{ @$data->whatsapp_cipta }}" class="whatsapp"><i class="bx bxl-whatsapp"></i></a>
							@endif
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="container py-4">
			<div class="copyright">
				{{ date('Y') }} &copy; Created by <strong><span><a href="/">{{ env("APP_COPY_RIGHT") }}</a></span></strong>.
			</div>
		</div>
	</footer>


	<div id="preloader"></div>
	<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

	<!-- Vendor JS Files -->
	<script src="{{ asset('web/vendor/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('web/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ asset('web/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
	<script src="{{ asset('web/vendor/php-email-form/validate.js') }}"></script>
	<script src="{{ asset('web/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('web/vendor/counterup/counterup.min.js') }}"></script>
	<script src="{{ asset('web/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('web/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
	<script src="{{ asset('web/vendor/venobox/venobox.min.js') }}"></script>
	<script src="{{ asset('web/vendor/aos/aos.js') }}"></script>
	<script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>

	<!-- Template Main JS File -->
	<script src="{{ asset('web/js/main.js') }}"></script>

	{{-- Highcharts --}}
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/data.js"></script>
	<script src="https://code.highcharts.com/modules/drilldown.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script src="https://code.highcharts.com/modules/accessibility.js"></script>


	@yield('contentJs')
</body>
</html>