<!DOCTYPE html>
<html lang="en">

<head>
  	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ env("APP_NAME") }} | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="icon" type="image/png" href="images/logophoto.png"> -->

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('original/old/css/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('original/old/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('original/old/styles/main.css') }}" rel="stylesheet">

    <link href="{{ asset('original/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{ asset('original/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="{{ asset('original/css/agency.min.css') }}" rel="stylesheet">

	@yield('contentCss')
</head>

<body id="top">
    <header>
      <div class="profile-page sidebar-collapse">
        <nav class="navbar navbar-expand-lg fixed-top navbar-transparent bg-primary" color-on-scroll="400">
          <div class="container">
            <div class="navbar-translate"><a class="navbar-brand" href="#" rel="tooltip">Portofolio</a>
              <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-bar bar1"></span><span class="navbar-toggler-bar bar2"></span><span class="navbar-toggler-bar bar3"></span></button>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navigation">
              <ul class="navbar-nav">
                <li class="nav-item"><a class="nav-link smooth-scroll" href="#about">About</a></li>
                <li class="nav-item"><a class="nav-link smooth-scroll" href="#skill">Skills</a></li>
                
                <li class="nav-item"><a class="nav-link smooth-scroll" href="#experience">Project</a></li>
                <li class="nav-item"><a class="nav-link smooth-scroll" href="#portfolio">Work Experience</a></li>
                <li class="nav-item"><a class="nav-link smooth-scroll" href="#contact">Contact</a></li>
                <li class="nav-item"><a class="nav-link smooth-scroll" href="https://gontangprakasa02.blogspot.com" target="_blank">Blog</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </header>

	<!-- ======= Hero Section ======= -->
	<section class="align-items-center"></section>
  	<!-- End Hero -->

  	@yield('content')

	<footer id="footer">
		<div class="footer-top">
			<div class="container">
				<div class="row">

					<div class="col-lg-4 col-md-6 footer-contact">
						<h3>Data Pribadi</h3>
						<p>
						{{ @$data->alamat_cipta }}<br><br>
						{{ @$data->negara_cipta }} <br><br>
						<strong>Phone :</strong> {{ @$data->telp_cipta }}<br>
						<strong>Email :</strong> {{ @$data->email_cipta }}<br>
						</p>
					</div>

					<div class="col-lg-4 col-md-6 footer-links">
						
					</div>

					<div class="col-lg-4 col-md-6 footer-links">
						<h4>Sosial Media</h4>
						<div class="social-links mt-3">
							@if (@$data->twitter_cipta)
								<a href="{{ @$data->twitter_cipta }}" class="twitter" target="_blank"><i class="bx bxl-twitter"></i></a>
							@endif
							@if (@$data->facebook_cipta)
								<a href="{{ @$data->facebook_cipta }}" class="facebook"><i class="bx bxl-facebook"></i></a>
							@endif
							@if (@$data->instagram_cipta)
								<a href="{{ @$data->instagram_cipta }}" class="instagram"><i class="bx bxl-instagram"></i></a>
							@endif
							@if (@$data->linkedin_cipta)
								<a href="{{ @$data->linkedin_cipta }}" class="linkedin"><i class="bx bxl-linkedin"></i></a>
							@endif
							@if (@$data->whatsapp_cipta)
								<a href="{{ @$data->whatsapp_cipta }}" class="whatsapp"><i class="bx bxl-whatsapp"></i></a>
							@endif
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="container py-4">
			<div class="copyright">
				{{ date('Y') }} &copy; Created by <strong><span><a href="/">{{ env("APP_COPY_RIGHT") }}</a></span></strong>.
			</div>
		</div>
	</footer>
</body>


	<div id="preloader"></div>
	<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

	<!-- Vendor JS Files -->
	<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/core/jquery.3.2.1.min.js"></script>
    <script src="{{ asset('original/old/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('original/old/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('original/old/js/plugins/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('original/old/js/now-ui-kit.js?v=1.1.0') }}"></script>
    <script src="{{ asset('original/old/js/aos.js') }}"></script>
    <script src="{{ asset('original/old/scripts/main.js') }}"></script>

    <script src="{{ asset('original/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('original/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ asset('original/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Contact form JavaScript -->
    <script src="{{ asset('original/js/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('original/js/contact_me.js') }}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{ asset('original/js/agency.min.js') }}"></script>


	@yield('contentJs')
</body>
</html>