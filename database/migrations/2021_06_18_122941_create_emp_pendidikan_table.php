<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpPendidikanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_pendidikan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('nama_pendidikan')->index('nama_pendidikan');
            $table->double('nilai_pendidikan')->index('nilai_pendidikan');
            $table->string('gelar_pendidikan')->index('gelar_pendidikan')->nullable();
            $table->date('mulai_pendidikan')->index('mulai_pendidikan');
            $table->date('selesai_pendidikan')->index('selesai_pendidikan');
            $table->string('bukti_pendidikan')->nullable();
            $table->text('deskripsi_pendidikan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_pendidikan');
    }
}
