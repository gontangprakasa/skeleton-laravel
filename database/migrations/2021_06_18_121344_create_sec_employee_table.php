<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSecEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sec_employee', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('kode_employee')->index('kode_employee');
            $table->string('nama_lengkap_employee')->index('nama_lengkap_employee')->nullable();
            $table->unsignedBigInteger('gender_employee_id')->index('gender_employee_id')->nullable();
            $table->string('nama_panggilan_employee')->index('nama_panggilan_employee')->nullable();
            $table->string('tempat_lahir_employee')->index('tempat_lahir_employee')->nullable();
            $table->date('tanggal_lahir_employee')->index('tanggal_lahir_employee')->nullable();
            $table->string('spesialisasi_employee')->index('spesialisasi_employee')->nullable();
            $table->string('no_telp_employee')->index('no_telp_employee')->nullable();
            $table->string('foto_employee')->nullable();
            $table->string('banner_employee')->nullable();
            $table->string('alamat_employee')->nullable();
            $table->string('cv_employee')->nullable();
            $table->string('portofolio_employee')->nullable();
            $table->text('tentang_employee')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sec_employee');
    }
}
