<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpPekerjaaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_pekerjaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('nama_pekerjaan')->index('akhir_organisasi');
            $table->unsignedBigInteger('jenis_pekerjaan_id')->index('jenis_pekerjaan_id');
            $table->string('lokasi_pekerjaan_id')->index('lokasi_pekerjaan_id');
            $table->text('alamat_pekerjaan');
            $table->string('posisi_pekerjaan')->index('posisi_pekerjaan');
            $table->date('mulai_pekerjaan')->index('mulai_pekerjaan');
            $table->date('selesai_pekerjaan')->index('selesai_pekerjaan');
            $table->string('bukti_pekerjaan')->nullable();
            $table->text('deskripsi_pekerjaan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_pekerjaan');
    }
}
