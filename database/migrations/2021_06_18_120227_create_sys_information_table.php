<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSysInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('alamat_cipta')->index('alamat_cipta');
            $table->string('negara_cipta')->index('negara_cipta');
            $table->string('telp_cipta')->index('telp_cipta');
            $table->string('email_cipta')->index('email_cipta');
            $table->string('twitter_cipta')->nullable()->index('twitter_cipta');
            $table->string('facebook_cipta')->nullable()->index('facebook_cipta');
            $table->string('linkedin_cipta')->nullable()->index('linkedin_cipta');
            $table->string('instagram_cipta')->nullable()->index('instagram_cipta');
            $table->string('whatsapp_cipta')->nullable()->index('whatsapp_cipta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_information');
    }
}
