<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptKeahlianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opt_keahlian', function (Blueprint $table) {
            $table->bigIncrements('keahlian_id');
            $table->string('nama_keahlian')->index('nama_keahlian');
            $table->string('konfirmasi_keahlian')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opt_keahlian');
    }
}
