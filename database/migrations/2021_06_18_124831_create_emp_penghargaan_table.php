<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpPenghargaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_penghargaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('nama_penghargaan')->index('nama_penghargaan');
            $table->string('lokasi_penghargaan')->index('lokasi_penghargaan');
            $table->string('gelar_penghargaan')->index('gelar_penghargaan')->nullable();
            $table->date('tanggal_penghargaan')->index('tanggal_penghargaan');
            $table->text('deskripsi_penghargaan')->nullable();
            $table->string('bukti_penghargaan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_penghargaan');
    }
}
