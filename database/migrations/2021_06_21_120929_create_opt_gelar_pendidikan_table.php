<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptGelarPendidikanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opt_gelar_pendidikan', function (Blueprint $table) {
            $table->bigIncrements('gelar_pendidikan_id');
            $table->string('nama_gelar')->index('nama_gelar');
            $table->string('konfirmasi_gelar')->index('konfirmasi_gelar')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opt_gelar_pendidikan');
    }
}
