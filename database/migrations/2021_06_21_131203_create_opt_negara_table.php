<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptNegaraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opt_negara', function (Blueprint $table) {
            $table->bigIncrements('negara_id');
            $table->string('nama_negara')->index('nama_negara');
            $table->string('nama_negara_asli')->index('nama_negara_asli');
            $table->string('kode_negara')->index('kode_negara')->nullable();
            $table->string('kode_negara_asli')->index('kode_negara_asli')->nullable();
            $table->integer('nomor_negara')->index('nomor_negara')->nullable();
            $table->string('notelp_negara')->index('notelp_negara')->nullable();
            $table->string('konfirmasi_negara')->index('konfirmasi_negara')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opt_negara');
    }
}
