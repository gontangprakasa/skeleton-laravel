<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpKeahlianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_keahlian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_keahlian');
            $table->unsignedBigInteger('employee_id');
            $table->integer('point_keahlian')->default(0);
            $table->string('bukti_keahlian')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_keahlian');
    }
}
