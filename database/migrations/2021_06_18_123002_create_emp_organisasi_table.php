<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpOrganisasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_organisasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('nama_organisasi')->index('nama_organisasi');
            $table->string('gelar_organisasi')->index('gelar_organisasi')->nullable();
            $table->date('mulai_organisasi')->index('mulai_organisasi');
            $table->date('selesai_organisasi')->index('selesai_organisasi');
            $table->string('bukti_organisasi')->nullable();
            $table->text('deskripsi_organisasi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_organisasi');
    }
}
