<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_project', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('nama_project')->index('nama_project');
            $table->unsignedBigInteger('jenis_project_id')->index('jenis_project_id');
            $table->string('lokasi_project')->index('lokasi_project')->nullable();
            $table->string('alamat_project')->index('alamat_project')->nullable();
            $table->text('link_project');
            $table->date('mulai_project')->index('mulai_project');
            $table->date('selesai_project')->index('selesai_project');
            $table->string('bukti_project')->nullable();
            $table->text('deskripsi_project')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_project');
    }
}
