<?php

use Illuminate\Database\Seeder;
use App\Models\OptSocialMedia;

class SosialMediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Items = [
            [
                'nama_social_media'     => 'Instagram',
            ],
            [
                'nama_social_media'     => 'Facebook',
            ],
            [
                'nama_social_media'     => 'WhatsApp Web',
            ],
            [
                'nama_social_media'     => 'Twitter',
            ],
            [
                'nama_social_media'     => 'LinkedIn',
            ],
        ];
        foreach ($Items as $Item) {
            OptSocialMedia::insert([
                'nama_social_media'     => $Item['nama_social_media']
            ]);
        }
    }
}
