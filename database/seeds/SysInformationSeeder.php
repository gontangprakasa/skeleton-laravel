<?php

use Illuminate\Database\Seeder;
use App\Models\SysInformation;


class SysInformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = SysInformation::first();
        if (is_null($check)) {
            SysInformation::insert([
                'alamat_cipta'     => 'Prawirotaman MG 3/631 RT 31 RW 08',
                'negara_cipta'     => 'Indonesia',
                'telp_cipta'       => '089608489982',
                'email_cipta'      => 'gontangprakasa02@gmail.com',
                'twitter_cipta'    => 'https://www.google.com/',
                'facebook_cipta'   => 'https://www.google.com/',
                'linkedin_cipta'   => 'https://www.google.com/',
                'instagram_cipta'  => 'https://www.google.com/',
                'whatsapp_cipta'   => NULL,
            ]);
        }
    }
}
