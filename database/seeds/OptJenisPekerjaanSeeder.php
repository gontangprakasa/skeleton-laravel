<?php

use Illuminate\Database\Seeder;
use App\Models\OptJenisPekerjaan;

class OptJenisPekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $JenisPekerjaans = [
            [
                'jenis_pekerjaan'   => 'Purnawaktu',
            ],
            [
                'jenis_pekerjaan'   => 'Paruh Waktu',
            ],
            [
                'jenis_pekerjaan'   => 'Wiraswasta',
            ],
            [
                'jenis_pekerjaan'   => 'Pekerja Lepas',
            ],
            [
                'jenis_pekerjaan'   => 'Kontrak',
            ],
            [
                'jenis_pekerjaan'   => 'Magang',
            ],
            [
                'jenis_pekerjaan'   => 'Seasonal',
            ]
        ];

        foreach ($JenisPekerjaans as $key => $JenisPekerjaan) {
            OptJenisPekerjaan::insert([
                'jenis_pekerjaan'   => $JenisPekerjaan['jenis_pekerjaan']
            ]);
        }
    }
}
