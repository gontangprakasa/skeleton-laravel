<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SysInformationSeeder::class);
        $this->call(OptPendidikanSeeder::class);
        $this->call(GenderTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SosialMediaSeeder::class);
        $this->call(OptJenisPekerjaanSeeder::class);
        $this->call(OptJenisProjectSeeder::class);
    }
}
