<?php

use Illuminate\Database\Seeder;
use App\Models\OptGender;
class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $GenderItems = [
            [
                'gender_name'   => 'Lainnya',
            ],
            [
                'gender_name'   => 'Laki-Laki',
            ],
            [
                'gender_name'   => 'Perempuan',
            ]
        ];

        foreach ($GenderItems as $key => $GenderItem) {
            OptGender::insert([
                'nama_gender'   => $GenderItem['gender_name']
            ]);
        }
    }
}
