<?php

use Illuminate\Database\Seeder;
use App\Models\OptRole;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            Roles Type
        */

        $RoleItems = [
            [
                'role_name'     => 'Super Admin',
            ],
            [
                'role_name'     => 'Employee',
            ],
        ];

        /*
           Add Role Items
        */
        foreach ($RoleItems as $RoleItem) {
            OptRole::insert([
                'nama_role'     => $RoleItem['role_name']
            ]);
        }
    }
}
