<?php

use Illuminate\Database\Seeder;
use App\Models\OptJenisProject;

class OptJenisProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $JenisProjects = [
            [
                'jenis_project'   => 'Freelance',
            ],
            [
                'jenis_project'   => 'Non Freelance',
            ]
        ];

        foreach ($JenisProjects as $key => $JenisProject) {
            OptJenisProject::insert([
                'jenis_project'   => $JenisProject['jenis_project']
            ]);
        }
    }
}
