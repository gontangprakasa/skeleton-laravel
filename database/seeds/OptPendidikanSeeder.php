<?php

use Illuminate\Database\Seeder;
use App\Models\OptPendidikan;

class OptPendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $PendidikanItems = [
            [
                'nama_pendidikan'   => 'Taman Kanak Kanak',
            ],
            [
                'nama_pendidikan'   => 'SD / Sederajat',
            ],
            [
                'nama_pendidikan'   => 'SMP / Sederajat',
            ],
            [
                'nama_pendidikan'   => 'SMA / SMK / Sederajat',
            ],
            [
                'nama_pendidikan'   => 'Diploma',
            ],
            [
                'nama_pendidikan'   => 'Sarjana',
            ],
            [
                'nama_pendidikan'   => 'Magister',
            ],
            [
                'nama_pendidikan'   => 'Doktor',
            ]
        ];

        foreach ($PendidikanItems as $key => $PendidikanItem) {
            OptPendidikan::insert([
                'nama_pendidikan'   => $PendidikanItem['nama_pendidikan']
            ]);
        }
    }
}
